This is an implementation of the Offline Learned Helper Picker method for auxiliary objective selection as per the following paper:

**Anton Bassin, Arina Buzdalova:
Selection of Auxiliary Objectives Using Landscape Features and Offline Learned Classifier.**

### Requirements
* jdk8
* maven

### How to run
1.  In command line run *mvn clean install*
2.  Go to *target/* directory
3.  run command: *java -jar OLHP-experiments.jar --help* to see the possible usages

*JobShopClassifierBuilderOLHP.java* and *TSPClassifierBuilderOLHP.java* files hold meta-classifier learning algorithm.
*JobShopRunnerOLHP.java* and *TSPRunnerOLHP.java* files hold an algorithm for solving the benchmark problems with NSGAII and OLHP method.

Directory *test_results* contain final evaluation experiment results.

The source code in Java is available under Apache 2.0 license.