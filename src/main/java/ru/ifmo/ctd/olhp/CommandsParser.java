
/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp;

import com.beust.jcommander.Parameter;

public class CommandsParser {

    @Parameter(names = {"--help", "-h"}, help = true)
    private boolean help = false;

    @Parameter(names = {"-problem", "-p"}, required = true, validateWith = ProblemTypeParameterValidator.class, description = "Name of the problem: TSP or JSSP")
    private String problem;

    @Parameter(names = {"-jsspDataFile", "-jd"}, description = "Path to the JSSP data file")
    private String jsspDataFilePath = Main.class.getResource("/jssp/jobshop.txt").getPath();

    @Parameter(names = {"-jsspTrList", "-jtl"}, description = "Path to the JSSP file with train instances list")
    private String jsspTrainListPath = Main.class.getResource("/jssp/train.txt").toExternalForm();

    @Parameter(names = {"-jsspTestList", "-jttl"}, description = "Path to the JSSP file with test instances list")
    private String jsspTestListPath = Main.class.getResource("/jssp/test.txt").toExternalForm();

    @Parameter(names = {"-jsspTrainData", "-jtd"}, description = "Path to the JSSP directory with serialized meta-classifier " +
            "and file with auxiliary objectives number")
    private String jsspTrainDataPath = "train_output/JSSP/article";

    @Parameter(names = {"-tspDataFile", "-td"}, description = "Path to the TSP data files directory")
    private String tspDataFileDir = Main.class.getResource("/tsp/instances").getPath();

    @Parameter(names = {"-tspTrList", "-ttl"}, description = "Path to the TSP file with train instances list")
    private String tspTrainListPath = Main.class.getResource("/tsp/train.txt").toString();

    @Parameter(names = {"-tspTestList", "-tttl"}, description = "Path to the TSP file with test instances list")
    private String tspTestListPath = Main.class.getResource("/tsp/test.txt").toExternalForm();

    @Parameter(names = {"-tspTrainData", "-ttd"}, description = "Path to the TSP directory with serialized meta-classifier " +
            "and file with auxiliary objectives number")
    private String tspTrainDataPath = "train_output/TSP/article";

    @Parameter(names = {"-train", "-tr"}, description = "Perform OLHP meta-classifier training")
    private boolean train = false;

    @Parameter(names = {"-test", "-r"}, description = "Perform solving problem instances with the OLHP approach")
    private boolean test = false;

    public boolean isHelp() {
        return help;
    }

    public String getProblem() {
        return problem;
    }

    public boolean isTrain() {
        return train;
    }

    public boolean isTest() {
        return test;
    }

    public String getJsspDataFilePath() {
        return jsspDataFilePath;
    }

    public String getJsspTrainListPath() {
        return jsspTrainListPath;
    }

    public String getTspDataFileDir() {
        return tspDataFileDir;
    }

    public String getTspTrainListPath() {
        return tspTrainListPath;
    }

    public String getJsspTestListPath() {
        return jsspTestListPath;
    }

    public String getJsspTrainDataPath() {
        return jsspTrainDataPath;
    }

    public String getTspTestListPath() {
        return tspTestListPath;
    }

    public String getTspTrainDataPath() {
        return tspTrainDataPath;
    }
}
