
/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp;

import com.beust.jcommander.JCommander;
import ru.ifmo.ctd.olhp.jssp.JobShopClassifierBuilderOLHP;
import ru.ifmo.ctd.olhp.jssp.JobShopRunnerOLHP;
import ru.ifmo.ctd.olhp.tsp.TSPClassifierBuilderOLHP;
import ru.ifmo.ctd.olhp.tsp.TSPRunnerOLHP;

public class Main {

    public static void main(String[] args) throws Exception {
        CommandsParser commandsParser = new CommandsParser();
        JCommander jCommander = new JCommander(commandsParser, args);

        jCommander.setProgramName("OLHP EVALUATION EXPERIMENTS");
        if (commandsParser.isHelp()) {
            jCommander.usage();
            return;
        }

        if (commandsParser.getProblem().equalsIgnoreCase("jssp")) {
            if (commandsParser.isTrain()) {
                JobShopClassifierBuilderOLHP jspBuilderOLHP = new JobShopClassifierBuilderOLHP();
                jspBuilderOLHP.runLearning(commandsParser.getJsspDataFilePath(), commandsParser.getJsspTrainListPath());
            } else if (commandsParser.isTest()) {
                JobShopRunnerOLHP jobShopRunnerOLHP = new JobShopRunnerOLHP();
                jobShopRunnerOLHP.run(commandsParser.getJsspDataFilePath(), commandsParser.getJsspTestListPath(), commandsParser.getJsspTrainDataPath());
            }
        } else {
            if (commandsParser.isTrain()) {
                TSPClassifierBuilderOLHP tspBuilderOLHP = new TSPClassifierBuilderOLHP();
                tspBuilderOLHP.runLearning(commandsParser.getTspDataFileDir(), commandsParser.getTspTrainListPath());
            } else if (commandsParser.isTest()) {
                TSPRunnerOLHP tspRunnerOLHP = new TSPRunnerOLHP();
                tspRunnerOLHP.run(commandsParser.getTspDataFileDir(), commandsParser.getTspTestListPath(), commandsParser.getTspTrainDataPath());
            }
        }


    }
}
