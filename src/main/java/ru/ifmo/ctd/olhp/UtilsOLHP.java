
/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.ArffSaver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UtilsOLHP {

    public static AbstractClassifier getClassifier() {
        RandomForest rf = new RandomForest();
        rf.setNumTrees(100);
        return rf;
    }

    public static void storeClassifier(Classifier classifier, String directoryPath) throws IOException {
        System.out.println("Saving Classifier");
        String fileName = directoryPath + "/Classifier.ser";
        FileOutputStream fileOut = new FileOutputStream(fileName);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(classifier);
        out.close();
        fileOut.close();
        System.out.println("Serialized data is saved in " + fileName);
    }

    public static void storeData(Instances data, String directoryPath) throws IOException {
        System.out.println("Saving Train Data");
        ArffSaver saver = new ArffSaver();
        saver.setInstances(data);
        String fileName = directoryPath + "/Dataset.arff";
        saver.setFile(new File(fileName));
        saver.writeBatch();
        System.out.println("Serialized data is saved in " + fileName);
    }

    public static void crossValidate(Classifier c1, Instances data, String directoryPath) throws Exception {
        System.out.println("Cross Validating Classifier");
        Evaluation eval = new Evaluation(data);
        eval.crossValidateModel(c1, data, 10, new Random(System.currentTimeMillis()));

        System.out.println();
        String statsFileName = directoryPath + "/ClassifierMetrics.txt";
        PrintWriter pw = new PrintWriter(statsFileName);

        double errorRate = eval.errorRate();
        System.out.println("Estimated Error Rate = " + String.format("%.2f", errorRate));
        pw.println("Error Rate = " + String.format("%.2f", errorRate));
        double precision = eval.precision(0);
        System.out.println("Precision = " + String.format("%.2f", precision));
        pw.println("Precision = " + String.format("%.2f", precision));
        double recall = eval.recall(0);
        System.out.println("Recall = " + String.format("%.2f", recall));
        pw.println("Recall = " + String.format("%.2f", recall));
        double fMeasure = eval.fMeasure(0);
        System.out.println("F-measure = " + String.format("%.2f", fMeasure));
        pw.println("F-measure = " + String.format("%.2f", fMeasure));

        pw.flush();
    }

    public static void storeEvaluatorsNumber(int helpersCount, String directoryPath) throws FileNotFoundException {
        System.out.println("Saving Helpers Quantity");
        String statsFileName = directoryPath + "/HelpersQuantity.txt";
        PrintWriter pw = new PrintWriter(statsFileName);
        pw.println(helpersCount);
        pw.flush();
    }

    public static void validateModel(String directoryPath) throws Exception {
        Instances data = readTrainData(directoryPath);
        data.setClassIndex(0);
        crossValidate(getClassifier(), data, directoryPath);
    }

    public static Instances readTrainData(String directoryPath) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(directoryPath + "/Dataset.arff"));
        return new Instances(reader);
    }

    public static AbstractClassifier readClassifier(String directoryPath) throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream(directoryPath + "/Classifier.ser");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        AbstractClassifier classifier = (AbstractClassifier) in.readObject();
        in.close();
        fileIn.close();
        return classifier;
    }

    public static int readHelpersQuant(String directoryPath) throws IOException, ClassNotFoundException {
        List<String> strings = Files.readAllLines(Paths.get(directoryPath + "/HelpersQuantity.txt"));
        return Integer.parseInt(strings.get(0));
    }

    public static Map<String, File> getFilesMapFromDir(String path) {
        File[] filesFromDir = getFilesFromDir(path);
        if (filesFromDir != null) {
            return Stream.of(filesFromDir).collect(Collectors.toMap(File::getName, f -> f));
        }
        return new HashMap<>();
    }

    public static File[] getFilesFromDir(String path) {
        File file = new File(path);
        if (!file.isDirectory()) {
            return null;
        }
        return file.listFiles(pathname -> !pathname.isDirectory());
    }

    public static Map<String, Integer> readProblemSetWithBest(String fileName) throws IOException, URISyntaxException {

        Map<String, Integer> result = new HashMap<>();

        try (Stream<String> stream = Files.lines(Paths.get(new URI(fileName)))) {
            stream.forEach(line -> {
                final String[] split = line.trim().split("\\s");
                result.put(split[0], Integer.valueOf(split[1]));
            });

        } catch (IOException e) {
            throw e;
        }
        return result;
    }

    public static List<String> readProblemList(String fileName) throws IOException, URISyntaxException {

        List<String> result = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(new URI(fileName)))) {
            stream.forEach(line -> {
                if (!line.trim().isEmpty()) {
                    result.add(line.trim());
                }
            });
        } catch (IOException e) {
            throw e;
        }
        return result;
    }

}
