/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core;

/**
 * An utility class for arrays containing methods not found in {@link java.util.Arrays}.
 *
 * @author Maxim Buzdalov
 */
public final class ArraysEx {
    private ArraysEx() {
        Static.doNotCreateInstancesOf(ArraysEx.class);
    }

    /**
     * Returns a full copy of the given two-dimensional array. If the argument is <tt>null</tt>,
     * then <tt>null</tt> is returned.
     *
     * @param a the array to be copied.
     * @return the copy of the given array.
     */
    public static boolean[][] copy(boolean[][] a) {
        if (a == null) {
            return null;
        }
        boolean[][] rv = a.clone();
        for (int i = 0, x = rv.length; i < x; ++i) {
            rv[i] = rv[i].clone();
        }
        return rv;
    }

    /**
     * Returns a full copy of the given two-dimensional array. If the argument is <tt>null</tt>,
     * then <tt>null</tt> is returned.
     *
     * @param a the array to be copied.
     * @return the copy of the given array.
     */
    public static byte[][] copy(byte[][] a) {
        if (a == null) {
            return null;
        }
        byte[][] rv = a.clone();
        for (int i = 0, x = rv.length; i < x; ++i) {
            rv[i] = rv[i].clone();
        }
        return rv;
    }

    /**
     * Returns a full copy of the given two-dimensional array. If the argument is <tt>null</tt>,
     * then <tt>null</tt> is returned.
     *
     * @param a the array to be copied.
     * @return the copy of the given array.
     */
    public static char[][] copy(char[][] a) {
        if (a == null) {
            return null;
        }
        char[][] rv = a.clone();
        for (int i = 0, x = rv.length; i < x; ++i) {
            rv[i] = rv[i].clone();
        }
        return rv;
    }

    /**
     * Returns a full copy of the given two-dimensional array. If the argument is <tt>null</tt>,
     * then <tt>null</tt> is returned.
     *
     * @param a the array to be copied.
     * @return the copy of the given array.
     */
    public static short[][] copy(short[][] a) {
        if (a == null) {
            return null;
        }
        short[][] rv = a.clone();
        for (int i = 0, x = rv.length; i < x; ++i) {
            rv[i] = rv[i].clone();
        }
        return rv;
    }

    /**
     * Returns a full copy of the given two-dimensional array. If the argument is <tt>null</tt>,
     * then <tt>null</tt> is returned.
     *
     * @param a the array to be copied.
     * @return the copy of the given array.
     */
    public static int[][] copy(int[][] a) {
        if (a == null) {
            return null;
        }
        int[][] rv = a.clone();
        for (int i = 0, x = rv.length; i < x; ++i) {
            rv[i] = rv[i].clone();
        }
        return rv;
    }

    /**
     * Returns a full copy of the given two-dimensional array. If the argument is <tt>null</tt>,
     * then <tt>null</tt> is returned.
     *
     * @param a the array to be copied.
     * @return the copy of the given array.
     */
    public static long[][] copy(long[][] a) {
        if (a == null) {
            return null;
        }
        long[][] rv = a.clone();
        for (int i = 0, x = rv.length; i < x; ++i) {
            rv[i] = rv[i].clone();
        }
        return rv;
    }

    /**
     * Returns a full copy of the given two-dimensional array. If the argument is <tt>null</tt>,
     * then <tt>null</tt> is returned.
     *
     * @param a the array to be copied.
     * @return the copy of the given array.
     */
    public static float[][] copy(float[][] a) {
        if (a == null) {
            return null;
        }
        float[][] rv = a.clone();
        for (int i = 0, x = rv.length; i < x; ++i) {
            rv[i] = rv[i].clone();
        }
        return rv;
    }

    /**
     * Returns a full copy of the given two-dimensional array. If the argument is <tt>null</tt>,
     * then <tt>null</tt> is returned.
     *
     * @param a the array to be copied.
     * @return the copy of the given array.
     */
    public static double[][] copy(double[][] a) {
        if (a == null) {
            return null;
        }
        double[][] rv = a.clone();
        for (int i = 0, x = rv.length; i < x; ++i) {
            rv[i] = rv[i].clone();
        }
        return rv;
    }

    /**
     * Returns a full copy of the given two-dimensional array. If the argument is <tt>null</tt>,
     * then <tt>null</tt> is returned.
     * <p>
     * The objects in the given array are not cloned, just copied by reference.
     *
     * @param a the array to be copied.
     * @return the copy of the given array.
     */
    public static <T> T[][] copy(T[][] a) {
        if (a == null) {
            return null;
        }
        T[][] rv = a.clone();
        for (int i = 0, x = rv.length; i < x; ++i) {
            rv[i] = rv[i].clone();
        }
        return rv;
    }
}
