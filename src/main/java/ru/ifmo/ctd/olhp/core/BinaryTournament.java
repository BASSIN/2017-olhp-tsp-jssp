/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Binary tournament selection operator.
 * Two individuals are chosen randomly, then the one with the lowest squeeze factor is taken.
 *
 * @param <T> the type of an individual
 * @author Arina Buzdalova
 * @see HyperBox
 */
public class BinaryTournament<T> implements Selection<T> {

    /**
     * {@inheritDoc}
     */
    @Override
    public EvaluatedIndividual<T> select(
            Collection<EvaluatedIndividual<T>> population, Random rng) {
        List<EvaluatedIndividual<T>> list = new ArrayList<>(population);
        EvaluatedIndividual<T> first = list.get(rng.nextInt(list.size()));
        EvaluatedIndividual<T> second = list.get(rng.nextInt(list.size()));
        return first.getSqueezeFactor() < second.getSqueezeFactor() ? first : second;
    }

}
