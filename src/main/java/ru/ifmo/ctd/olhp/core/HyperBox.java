/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * The hyper-box -- element of a hyper-grid that stores evaluated individuals.
 * All the individuals in the hyper-box have the same squeeze factor,
 * which is equal to the number of individuals in the hyper-box.
 *
 * @param <T> the type of an individual
 * @author Arina Buzdalova
 */
public class HyperBox<T> implements Comparable<HyperBox<? extends T>> {
    private final List<EvaluatedIndividual<T>> individuals;

    /**
     * Constructs {@link HyperBox} without any parameters.
     */
    public HyperBox() {
        individuals = new ArrayList<>();
    }

    /**
     * Returns an unmodifiable view on all the individuals contained in this hyper-box.
     *
     * @return an unmodifiable view on all the individuals contained in this hyper-box
     */
    public List<EvaluatedIndividual<T>> getAll() {
        return Collections.unmodifiableList(individuals);
    }

    /**
     * Adds the specified individual to this hyper-box.
     *
     * @param individual the individual to be added
     */
    public void add(EvaluatedIndividual<T> individual) {
        individual.setSqueezeFactor(getSqueezeFactor());
        individuals.add(individual);
        updateFactors();
    }

    private void updateFactors() {
        for (EvaluatedIndividual<T> ind : individuals) {
            ind.incSqueezeFactor();
        }
    }

    /**
     * Checks whether this hyper-box is empty.
     *
     * @return {@code true}, if this hyper-box is empty, {@code false} otherwise
     */
    public boolean isEmpty() {
        return individuals.size() == 0;
    }

    /**
     * Removes a random individual from this hyper-box and returns it.
     *
     * @param rng the source of randomness
     * @return {@code null} if the box is empty, the removed individual otherwise
     */
    public EvaluatedIndividual<T> extractRandom(Random rng) {
        if (isEmpty()) {
            return null;
        }
        EvaluatedIndividual<T> ind = individuals.get(rng.nextInt(individuals.size()));
        individuals.remove(ind);
        return ind;
    }

    /**
     * Gets a random individual from this hyper-box, the individual stays in the hyper-box
     *
     * @param rng the source of randomness
     * @return a random individual form this hyper-box
     */
    public EvaluatedIndividual<T> getRandom(Random rng) {
        return individuals.get(rng.nextInt(individuals.size()));
    }

    /**
     * Gets the current squeeze factor of this hyper-box.
     * Squeeze factor is the number of individuals consisted in a hyper-box.
     *
     * @return the current squeeze factor of this hyper-box
     */
    public int getSqueezeFactor() {
        return individuals.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(HyperBox<? extends T> o) {
        int sf = getSqueezeFactor();
        int osf = o.getSqueezeFactor();

        if (sf == osf) {
            return 0;
        }

        if (sf > osf) {
            return 1;
        }

        return -1;
    }
}
