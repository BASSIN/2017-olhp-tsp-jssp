/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core;

import org.uncommons.watchmaker.framework.FitnessEvaluator;

import java.util.List;

/**
 * Interface for multicriteria evolutionary algorithms
 * <p>
 * The individuals being evolved are expected to be string-based.
 *
 * @param <I> the type of an individual
 * @author Irene Petrova
 * @see MulticriteriaOptimizationAlgorithm
 */

public interface MulticriteriaAlgorithm<I> extends MulticriteriaOptimizationAlgorithm {


    /**
     * Makes this algorithm forgetting about already evolved individuals.
     * The algorithm will start from scratch after the next attempt to evolve individuals.
     */
    public void refresh();

    /**
     * Adds the {@link ru.ifmo.ctd.ngp.demo.ffchooser.genetic.Printer} used to observe this algorithm running
     *
     * @param printer the printer used to observe this algorithm running
     */
    public void addPrinter(Printer<? super I> printer);

    /**
     * Stops retrieving information to the specified printer
     *
     * @param printer the printer to be removed
     */
    public void removePrinter(Printer<? super I> printer);

    /**
     * Sets the population, which will be used to generate individuals
     * at the next iteration of the algorithm
     *
     * @param seedPopulation the start population
     */
    public void setStartPopulation(List<I> seedPopulation);

    /**
     * Sets the length of an individual and refreshes the algorithm
     * using the {@link #refresh} method
     *
     * @param length the length of an individual
     */
    public void setLength(int length);

    /**
     * Sets the specified fitness evaluator to the specified index.
     * Values of this evaluator will be returned by the {@link #computeValues()} method
     * under the corresponding index.
     *
     * @param index     the specified index
     * @param evaluator the evaluator to be associated with the <code>index</code>
     */
    public void setEvaluator(int index, FitnessEvaluator<? super I> evaluator);

    /**
     * Gets the name of algorithm
     *
     * @return the name of algorithm
     */
    public String getName();
}

