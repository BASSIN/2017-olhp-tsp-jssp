/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core;

import java.util.List;

/**
 */
public interface MulticriteriaOptimizationAlgorithm extends OptimizationAlgorithm {
    /**
     * Gets all the parameters values of the best (in terms of the current criterion) point
     *
     * @return all the parameters values of the best point
     */
    public List<List<Double>> getCurrentParetoFront();

    public List<List<Double>> getCurrentInternalGeneration();

    public List<Double> computeGenerationQuality(int generation);

    public void genGeneration(int helper);

    public List<List<Double>> computeParetoOfGeneration(int generation);

    public void setPopulation(int population);

    public void computeValuesOfGeneration(int generation);

    public double getFinalBestTargetValue();
}
