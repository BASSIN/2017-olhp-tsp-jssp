/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core;

import java.io.IOException;
import java.util.List;

/**
 * Interface for printers used to observe {@link EvolutionaryAlgImpl} running.
 *
 * @param <T> the type of an individual
 * @author Arina Buzdalova
 */
public interface Printer<T> {

    /**
     * Prints information about current state of evolution in the {@link EvolutionaryAlgImpl}
     *
     * @param values            the values to be printed,
     *                          typically list of best individual's fitness calculated by
     *                          different evaluators
     * @param bestIndividual    the best individual in the last evolved population
     * @param iterations        of the last iteration of genetic algorithm
     * @param curEvaluatorIndex index of the current fitness function
     */
    public void print(
            List<Double> values,
            T bestIndividual,
            int iterations,
            int curEvaluatorIndex);

    /**
     * Prints the specified information and a newline
     *
     * @param info the specified information
     * @throws IOException if an I/O exception occurs
     */
    public void println(String info) throws IOException;
}
