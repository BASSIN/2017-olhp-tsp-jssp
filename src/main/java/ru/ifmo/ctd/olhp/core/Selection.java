/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core;

import java.util.Collection;
import java.util.Random;

/**
 * The selection operator interface.
 *
 * @param <T> type of an individual
 * @author Arina Buzdalova
 */
public interface Selection<T> {

    /**
     * Selects one individual from the specified population
     * using the implemented selection strategy.
     *
     * @param population the specified population
     * @param rng        the source of randomness
     * @return an individual selected from the {@code population}
     */
    public EvaluatedIndividual<T> select(Collection<EvaluatedIndividual<T>> population, Random rng);
}
