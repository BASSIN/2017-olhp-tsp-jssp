/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core;

/**
 * An utility class for "static" classes (e.g. containing only static methods).
 *
 * @author Maxim Buzdalov
 */
public final class Static {
    private Static() {
        Static.doNotCreateInstancesOf(Static.class);
    }

    /**
     * Must be called from within the private constructor of a "static" class.
     *
     * @param clazz the class.
     */
    public static void doNotCreateInstancesOf(Class<?> clazz) {
        throw new AssertionError("Do not create instances of class " + clazz.getName());
    }
}
