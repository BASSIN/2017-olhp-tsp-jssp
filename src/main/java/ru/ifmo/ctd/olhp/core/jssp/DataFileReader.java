/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core.jssp;


import ru.ifmo.ctd.olhp.core.ArraysEx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class DataFileReader {
    public static class InputDataSet {
        private InputDataSet(int[][] times, int[][] machines, String name) {
            this.times = times;
            this.machines = machines;
            this.name = name;
        }

        private final int[][] times;
        private final int[][] machines;
        private final String name;

        public int[][] getTimes() {
            return ArraysEx.copy(times);
        }

        public int[][] getMachines() {
            return ArraysEx.copy(machines);
        }

        public String getName() {
            return name;
        }
    }

    private final Map<String, InputDataSet> dataSets = new HashMap<>();

    private void loadFrom(BufferedReader in) throws IOException {
        String line;
        while ((line = in.readLine()) != null) {
            line = line.trim();
            if (line.startsWith("instance")) {
                String id = line.substring("instance".length()).trim();
                for (int i = 0; i < 3; ++i) {
                    in.readLine();
                }
                line = in.readLine();
                if (line != null) {
                    line = line.trim();
                    int ws = line.indexOf(' ');
                    int rows = Integer.parseInt(line.substring(0, ws));
                    int cols = Integer.parseInt(line.substring(ws + 1));
                    int[][] machines = new int[rows][cols];
                    int[][] times = new int[rows][cols];
                    for (int i = 0; i < rows; ++i) {
                        StringTokenizer st = new StringTokenizer(in.readLine());
                        for (int j = 0; j < cols; ++j) {
                            machines[i][j] = Integer.parseInt(st.nextToken());
                            times[i][j] = Integer.parseInt(st.nextToken());
                        }
                    }
                    dataSets.put(id, new InputDataSet(times, machines, id));
                }
            }
        }
    }

    public InputDataSet get(String name) {
        return dataSets.get(name);
    }

    public int getSize() {
        return dataSets.size();
    }

    public Map<String, InputDataSet> getDataSets() {
        return dataSets;
    }

    public DataFileReader(File file) throws IOException {
        try (FileReader in = new FileReader(file);
             BufferedReader inr = new BufferedReader(in)) {
            loadFrom(inr);
        }
    }
}
