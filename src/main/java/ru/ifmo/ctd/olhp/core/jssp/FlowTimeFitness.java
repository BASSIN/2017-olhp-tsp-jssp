/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core.jssp;

import org.uncommons.watchmaker.framework.FitnessEvaluator;

import java.util.List;

/**
 * Fitness evaluator for the Job Shop Scheduling Problem.
 * It evaluates the total flow time.
 *
 * @author Arina Buzdalova
 */
public class FlowTimeFitness implements FitnessEvaluator<List<Integer>> {
    private final int[][] times;
    private final int[][] machines;
    private final int max;

    /**
     * Constructs the {@link FlowTimeFitness} with the specified parameters.
     *
     * @param max      the maximal flow time
     * @param times    the processing times for each operation
     * @param machines the machines corresponding to each operation
     */
    public FlowTimeFitness(int max, int[][] times, int[][] machines) {
        this.times = times;
        this.machines = machines;
        this.max = max;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getFitness(List<Integer> individual, List<? extends List<Integer>> population) {
        int sum = 0;
        int[] flowTimes = JobShopUtils.evalFlowTimes(individual, JobShopUtils.createJobsList(times, machines));
        for (int i = 0; i < times.length; i++) {
            sum += flowTimes[i];
        }
        return max - sum;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isNatural() {
        return true;
    }

}
