/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core.jssp;

import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class JobShopFactory extends AbstractCandidateFactory<List<Integer>> {
    private final int jobs;
    private final int machines;

    public JobShopFactory(int jobs, int machines) {
        this.jobs = jobs;
        this.machines = machines;
    }

    @Override
    public List<List<Integer>> generateInitialPopulation(int size, Random rng) {
        return JobShopUtils.generateRandomPopulation(size, jobs, machines, rng);
    }

    @Override
    public List<List<Integer>> generateInitialPopulation(int size,
                                                         Collection<List<Integer>> population, Random rng) {
        List<List<Integer>> newPop = new ArrayList<>(population);
        newPop.addAll(JobShopUtils.generateRandomPopulation(size - newPop.size(), jobs, machines, rng));
        return newPop;
    }

    @Override
    public List<Integer> generateRandomCandidate(Random rng) {
        return JobShopUtils.generateRandomIndividual(jobs, machines, rng);
    }

}
