/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core.jssp;

/**
 * Single operation of a job.
 *
 * @author Arina Buzdalova
 */
class Operation implements Comparable<Operation> {
    private final int number;
    private final int job;
    private final int machine;
    private final int time;
    private int completionTime;

    /**
     * Constructs the {@link Operation} with the specified parameters.
     *
     * @param number  the number of this operation in the corresponding job
     * @param job     the corresponding job
     * @param machine the machine that should perform this operation
     * @param time    the performance time of this operation
     */
    public Operation(int number, int job, int machine, int time) {
        this.number = number;
        this.job = job;
        this.machine = machine;
        this.time = time;
    }

    public int getCompletionTime() {
        return completionTime;
    }

    public void setCompletionTime(int completionTime) {
        this.completionTime = completionTime;
    }

    /**
     * @return the number of this operation in the job
     */
    public int getNumber() {
        return number;
    }

    /**
     * @return the job
     */
    public int getJob() {
        return job;
    }

    /**
     * @return the machine that should perform this operation
     */
    public int getMachine() {
        return machine;
    }

    /**
     * @return the processing time of this operation
     */
    public int getTime() {
        return time;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + job;
        result = prime * result + number;
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Operation other = (Operation) obj;
        return job == other.job && number == other.number;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(Operation o) {
        return getCompletionTime() - o.getCompletionTime();
    }

}
