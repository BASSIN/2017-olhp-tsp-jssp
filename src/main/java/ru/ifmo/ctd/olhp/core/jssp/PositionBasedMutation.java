/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core.jssp;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Position Based Mutation (PBM) operator for the Job Shop Problem.
 * It moves a randomly chosen element to a random position.
 *
 * @author Arina Buzdalova
 */
public class PositionBasedMutation implements EvolutionaryOperator<List<Integer>> {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<List<Integer>> apply(List<List<Integer>> population, Random rng) {
        List<List<Integer>> mutated = new ArrayList<>();
        for (List<Integer> individual : population) {
            mutated.add(mutate(individual, rng));
        }
        return mutated;
    }

    private List<Integer> mutate(List<Integer> individual, Random rng) {
        int len = individual.size();
        int pos1 = rng.nextInt(len);
        int pos2 = rng.nextInt(len);

        List<Integer> mutated = new ArrayList<>(individual);
        Integer removed = mutated.remove(pos1);
        mutated.add(pos2, removed);

        return mutated;
    }
}
