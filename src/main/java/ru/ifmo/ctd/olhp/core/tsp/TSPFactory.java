/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core.tsp;

import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * @author Irene Petrova
 */
public class TSPFactory extends AbstractCandidateFactory<List<Integer>> {
    private final TSPProblem problem;

    public TSPFactory(TSPProblem tsp) {
        this.problem = tsp;
    }

    @Override
    public List<List<Integer>> generateInitialPopulation(int size, Random rng) {
        return TSPUtils.generateRandomPopulation(size, problem.tsp, rng);
    }

    @Override
    public List<List<Integer>> generateInitialPopulation(int size,
                                                         Collection<List<Integer>> population, Random rng) {
        List<List<Integer>> newPop = new ArrayList<>(population);
        newPop.addAll(TSPUtils.generateRandomPopulation(size - newPop.size(), problem.tsp, rng));
        return newPop;
    }

    @Override
    public List<Integer> generateRandomCandidate(Random rng) {
        return TSPUtils.generateRandomIndividual(problem.tsp, rng);
    }

}
