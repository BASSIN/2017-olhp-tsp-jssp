/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core.tsp;

import org.uncommons.watchmaker.framework.FitnessEvaluator;

import java.util.List;

/**
 * @author Irene Petrova
 */
public class TSPFitness implements FitnessEvaluator<List<Integer>> {
    private final double max;
    private TSPProblem problem;

    public TSPFitness(TSPProblem problem) {
        this.max = problem.getMax();
        this.problem = problem;
    }

    @Override
    public double getFitness(List<Integer> individual, List<? extends List<Integer>> population) {
        double flowTimes = TSPUtils.evalTime(individual, problem.tsp);
        if (flowTimes == Double.POSITIVE_INFINITY) {
            flowTimes = 0;
        }
        return max - flowTimes;
    }


    @Override
    public boolean isNatural() {
        return true;
    }

}

