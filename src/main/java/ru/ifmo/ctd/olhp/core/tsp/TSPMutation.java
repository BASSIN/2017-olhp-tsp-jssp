/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core.tsp;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Irene Petrova
 */
public class TSPMutation implements EvolutionaryOperator<List<Integer>> {

    @Override
    public List<List<Integer>> apply(List<List<Integer>> population, Random rng) {
        List<List<Integer>> mutated = new ArrayList<>();
        for (List<Integer> individual : population) {
            mutated.add(mutate(individual, rng));
        }
        return mutated;
    }

    public List<Integer> mutate(List<Integer> individual, Random rng) {
        int len = individual.size();
        int pos1 = rng.nextInt(len);
        int pos2 = rng.nextInt(len);
        while (pos2 == pos1) {
            pos2 = rng.nextInt(len);
        }

        List<Integer> mutated = new ArrayList<>(individual);
        int segment = pos2 > pos1 ? pos2 - pos1 + 1 : pos2 - pos1 + len + 1;
        for (int i = 0; i <= segment / 2 - 1; ++i) {
            int curPos1 = (pos1 + i) % len;
            int curPos2 = (pos2 - i) % len;
            if (curPos2 < 0) {
                curPos2 += len;
            }

            int tmp = mutated.get(curPos1);
            mutated.set(curPos1, mutated.get(curPos2));
            mutated.set(curPos2, tmp);
        }

        return mutated;
    }
}
