/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.core.tsp;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;
import ru.ifmo.ctd.olhp.core.CollectionsEx;
import ru.ifmo.ctd.olhp.core.EvaluatedIndividual;
import ru.ifmo.ctd.olhp.core.NSGA2Multicriteria;
import ru.ifmo.ctd.olhp.core.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author Irene Petrova
 */
public class TSPNSGA2Multicriteria extends NSGA2Multicriteria<List<Integer>> {

    private TSPProblem problem;

    public TSPNSGA2Multicriteria(FitnessEvaluator<List<Integer>> targetCriterion, List<FitnessEvaluator<List<Integer>>> helpers, AbstractCandidateFactory<List<Integer>> factory, EvolutionaryOperator<List<Integer>> mutation, EvolutionaryOperator<List<Integer>> crossover, double crossoverProbability, int generationSize, Random rng, TSPProblem problem) {
        super(targetCriterion, helpers, factory, mutation, crossover, crossoverProbability, generationSize, rng);
        this.problem = problem;
    }

    @Override
    public List<Individual<List<Integer>>> genChildren(List<EvaluatedIndividual<List<Integer>>> selected) {
        List<Individual<List<Integer>>> generatedIndividuals = new ArrayList<>();
        for (int i = 0; i < selected.size(); ++i) {
            List<Integer> p;
            if (crossoverProbability.nextEvent(rng)) {
                p = crossover.apply(CollectionsEx.listOf(
                        selection.select(selected, rng).ind(),
                        selection.select(selected, rng).ind()
                ), rng).get(0);
            } else {
                p = selection.select(selected, rng).ind();
                p = mutation.apply(CollectionsEx.listOf(p), rng).get(0);
            }
            TSPUtils.apply2Opt(p, problem);
            Individual<List<Integer>> child = new Individual<>(p, Utils.evaluate(p, CollectionsEx.listOf(p), criteria));
            generatedIndividuals.add(child);
        }
        return generatedIndividuals;
    }
}
