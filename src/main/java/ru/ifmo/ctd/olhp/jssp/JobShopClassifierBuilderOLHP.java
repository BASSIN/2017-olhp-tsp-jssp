/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.jssp;

import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;
import ru.ifmo.ctd.olhp.UtilsOLHP;
import ru.ifmo.ctd.olhp.core.FastRandom;
import ru.ifmo.ctd.olhp.core.NSGA2MulticriteriaSlow;
import ru.ifmo.ctd.olhp.core.jssp.DataFileReader;
import ru.ifmo.ctd.olhp.core.jssp.FlowTimeFitness;
import ru.ifmo.ctd.olhp.core.jssp.GeneralizedOrderCrossover;
import ru.ifmo.ctd.olhp.core.jssp.JobShopFactory;
import ru.ifmo.ctd.olhp.core.jssp.JobShopUtils;
import ru.ifmo.ctd.olhp.core.jssp.PositionBasedMutation;
import weka.classifiers.AbstractClassifier;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class JobShopClassifierBuilderOLHP {
    private static final Random RANDOM = FastRandom.threadLocal();
    private static final int HELPERS_COUNT = 4;
    private static final int NUMBER_OF_RUNS = 4;
    public static final int POPULATION_SIZE = 100;
    public static final double CROSSOVER_PROBABILITY = 0.8;

    private static Instances trainData;

    private static AbstractClassifier classifier;

    public static void main(String[] args) throws Exception {

        String directory = "./train_output/JSSP/article";
        AbstractClassifier classifier = UtilsOLHP.readClassifier(directory);
        Instances data = UtilsOLHP.readTrainData(directory);
        data.setClassIndex(0);

        UtilsOLHP.crossValidate(classifier, data, "./train_output");
    }

    public static void runLearning(String dataFilePath, String trainInstancesFilePath) throws Exception {

        final DataFileReader reader = new DataFileReader(new File(dataFilePath));
        final Map<String, Integer> trainProblemsMap = UtilsOLHP.readProblemSetWithBest(trainInstancesFilePath);

        classifier = UtilsOLHP.getClassifier();
        trainData = JobshopFeaturesUtility.getEmptyTrainInstances(HELPERS_COUNT);

        long startTime = System.currentTimeMillis();

        AtomicInteger instancesLeft = new AtomicInteger(trainProblemsMap.size());

        trainProblemsMap.forEach((instance, bestKnown) -> {
            System.out.println("Processing " + instance + ". Files left = " + instancesLeft.decrementAndGet());
            System.out.println("Total runs quantity =  " + NUMBER_OF_RUNS);
            try {
                runTraining(reader.get(instance), bestKnown, instance);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        long endTime = System.currentTimeMillis();
        System.out.println("TOTAL TIME = [" + UtilJobShop.formatTime(endTime - startTime) + "]");

        System.out.println("Randomizing dataset");
        trainData.randomize(RANDOM);

        System.out.println("Number of dataset vectors = [" + trainData.numInstances() + "]");

        System.out.println("Building classifier");
        classifier.buildClassifier(trainData);

        Date date = new Date();
        String directoryPath = "./train_output/JSSP/" + new SimpleDateFormat("yy-MM-dd-HH-mm-ss").format(date);
        new File(directoryPath).mkdirs();

        UtilsOLHP.storeClassifier(classifier, directoryPath);
        UtilsOLHP.storeEvaluatorsNumber(HELPERS_COUNT, directoryPath);
        UtilsOLHP.storeData(trainData, directoryPath);

        UtilsOLHP.crossValidate(classifier, trainData, directoryPath);
    }


    private static void runTraining(DataFileReader.InputDataSet inputData, int bestResult, String instance) throws IOException, InterruptedException, ExecutionException {

        long startTime = System.currentTimeMillis();

        final int generations = UtilJobShop.calculateTotalIterations(inputData);
        final int[][] times = inputData.getTimes();
        final int[][] machines = inputData.getMachines();
        final int jobs = times.length;
        final int max = jobs * JobShopUtils.sumTimes(times);

        final double[] flowtime = UtilJobShop.getMinFlowtime(times);
        final int[] sortedHelpers = UtilJobShop.sortedByFlowtime(flowtime);

        List<FitnessEvaluator<List<Integer>>> evaluators = UtilJobShop.getEvaluators(HELPERS_COUNT, sortedHelpers, max, times, machines);

        final double[] metaFeatures = JobshopFeaturesUtility.computeMetaFeatures(inputData);


        AtomicInteger runNumber = new AtomicInteger(0);

        IntStream.range(0, NUMBER_OF_RUNS).parallel().forEach(i -> {
            final String logPrefix = "Problem [" + instance + "] Run [" + runNumber.incrementAndGet() + "] ";
            System.out.println(logPrefix + "Current time: " + new Date().toString());
            long runStartTime = System.currentTimeMillis();


            AbstractCandidateFactory<List<Integer>> factory = new JobShopFactory(jobs, times[0].length);
            FitnessEvaluator<List<Integer>> targetFitness = new FlowTimeFitness(max, times, machines);
            EvolutionaryOperator<List<Integer>> mutation = new PositionBasedMutation();
            NSGA2MulticriteriaSlow<List<Integer>> multicriteriaAlgo = new NSGA2MulticriteriaSlow<>(targetFitness, evaluators,
                    factory, mutation, new GeneralizedOrderCrossover(new Probability(CROSSOVER_PROBABILITY), jobs), CROSSOVER_PROBABILITY, POPULATION_SIZE, FastRandom.threadLocal());

            int iterPerHelper = (generations + evaluators.size() - 1) / evaluators.size();

            int currGenerationsCount = 0;
            //todo concurrency
            while (currGenerationsCount < generations) {

                List<NSGA2MulticriteriaSlow<List<Integer>>.Individual<List<Integer>>> oldGeneration = multicriteriaAlgo.getCurrentGeneration();
                int oldIterations = multicriteriaAlgo.getIterationsNumber();

                double bestFitness = Double.NEGATIVE_INFINITY;
                List<NSGA2MulticriteriaSlow<List<Integer>>.Individual<List<Integer>>> bestGeneration = null;
                int bestIterations = -1;
                int bestCriteria = -1;

                for (int k = 0; k < evaluators.size(); k++) {
                    multicriteriaAlgo.changeCriterion(k);
                    for (int j = 0; j < iterPerHelper; ++j) {
                        multicriteriaAlgo.computeValues();
                    }

                    double bestTargetValue = multicriteriaAlgo.getCurrentBest().get(0);
                    //!!! or algo.getBestTargetValue();

                    if (bestTargetValue > bestFitness) {
                        bestCriteria = k;
                        bestIterations = multicriteriaAlgo.getIterationsNumber();
                        bestGeneration = multicriteriaAlgo.getCurrentGeneration();
                        bestFitness = bestTargetValue;
                    }
                    if (k < evaluators.size() - 1) {
                        multicriteriaAlgo.restoreToState(oldGeneration, oldIterations);
                    }
                }

                addFeaturesVector(bestCriteria, metaFeatures, oldGeneration, currGenerationsCount, inputData, bestResult);


                currGenerationsCount += iterPerHelper;
                multicriteriaAlgo.restoreToState(bestGeneration, bestIterations);
            }

            long endTime = System.currentTimeMillis();
            long totalTime = endTime - runStartTime;
            String format = UtilJobShop.formatTime(totalTime);
            System.out.println(logPrefix + " time_taken = [" + format + "]");


        });

        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        String format = UtilJobShop.formatTime(totalTime);
        System.out.println("trainFile = [" + instance + "], time_taken_total = [" + format + "]");

    }

    private static synchronized void addFeaturesVector(int currCriteria, double[] metaFeatures, List<NSGA2MulticriteriaSlow<List<Integer>>.Individual<List<Integer>>> currentGeneration, int generationsCount, DataFileReader.InputDataSet inputData, double bestKnown) {
        double[] allFeatures = JobshopFeaturesUtility.computeAllFeatures(currCriteria, metaFeatures, currentGeneration, generationsCount, inputData, bestKnown);
        Instance instance = new DenseInstance(1, allFeatures);
        trainData.add(instance);
    }
}
