/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.jssp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.uncommons.maths.random.Probability;
import org.uncommons.maths.statistics.DataSet;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;
import ru.ifmo.ctd.olhp.UtilsOLHP;
import ru.ifmo.ctd.olhp.core.FastRandom;
import ru.ifmo.ctd.olhp.core.NSGA2MulticriteriaSlow;
import ru.ifmo.ctd.olhp.core.jssp.DataFileReader;
import ru.ifmo.ctd.olhp.core.jssp.FlowTimeFitness;
import ru.ifmo.ctd.olhp.core.jssp.GeneralizedOrderCrossover;
import ru.ifmo.ctd.olhp.core.jssp.JobShopFactory;
import ru.ifmo.ctd.olhp.core.jssp.JobShopUtils;
import ru.ifmo.ctd.olhp.core.jssp.PositionBasedMutation;
import ru.ifmo.ctd.olhp.tsp.FeatureUtility;
import weka.classifiers.AbstractClassifier;
import weka.core.DenseInstance;
import weka.core.Instances;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class JobShopRunnerOLHP {

    private static final double CROSSOVER_PROBABILITY = 0.8;
    public static final int POPULATION_SIZE = 100;
    public static final int RUNS = 100;

    public static void run(String dataFilePath, String testInstancesFilePath, String trainDataPath) throws IOException, ClassNotFoundException, URISyntaxException {

        int helpersQuant = UtilsOLHP.readHelpersQuant(trainDataPath);
        AbstractClassifier classifier = UtilsOLHP.readClassifier(trainDataPath);


        final DataFileReader reader = new DataFileReader(new File(dataFilePath));
        final Map<String, Integer> testProblemsMap = UtilsOLHP.readProblemSetWithBest(testInstancesFilePath);


        String outPutDirectoryPath = "./test_results/JSSP/" + new SimpleDateFormat("yy-MM-dd-HH-mm-ss").format(new Date());
        new File(outPutDirectoryPath).mkdirs();

        final int totalProblems = testProblemsMap.size();
        AtomicInteger problemsLeft = new AtomicInteger(totalProblems);
        testProblemsMap.forEach((instance, bestKnown) ->
        {
            try {
                System.out.println("Processing " + instance + ". problems left = " + (problemsLeft.decrementAndGet()));

                runOLHP(reader.get(instance), bestKnown, instance, helpersQuant, classifier, outPutDirectoryPath, POPULATION_SIZE, RUNS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private static void runOLHP(DataFileReader.InputDataSet problem, int bestKnown, String problemName, final int helpersQuant, AbstractClassifier classifier, String outPutDirectoryPath, int populationSize, int runs) throws Exception {
        PrintWriter pw = new PrintWriter(outPutDirectoryPath + "/" + problemName + "-" + new SimpleDateFormat("yy-MM-dd-HH-mm-ss").format(new Date()) + "-OLHP.json");
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("problem_name", problemName);
        jsonObject.addProperty("method", "OLHP");

        final int totalGenerations = UtilJobShop.calculateTotalIterations(problem);

        final int[][] times = problem.getTimes();
        final int[][] machines = problem.getMachines();
        final int jobs = times.length;
        final int max = jobs * JobShopUtils.sumTimes(times);

        final double[] flowtime = UtilJobShop.getMinFlowtime(times);
        final int[] sortedHelpers = UtilJobShop.sortedByFlowtime(flowtime);
        List<FitnessEvaluator<List<Integer>>> evaluators = UtilJobShop.getEvaluators(helpersQuant, sortedHelpers, max, times, machines);


        double[] metaFeatures = JobshopFeaturesUtility.computeMetaFeatures(problem);
        System.out.println(String.format("Problem = [" + problemName + "] Optimal answer = " + bestKnown));
        jsonObject.add("optimal_answer", new JsonPrimitive(bestKnown));
        DataSet dataSet = new DataSet();
        AtomicInteger optimalFoundTimes = new AtomicInteger(0);
        List<Integer> bestFoundAt = new CopyOnWriteArrayList<>(Collections.nCopies(runs, -1));

        List<Double> runResults = new CopyOnWriteArrayList<>();
        AtomicInteger runNumber = new AtomicInteger(0);
        Object sync = new Object();
        IntStream.range(0, runs).parallel().forEach(i -> {
            boolean foundOptimal = false;
            double bestOnRun = Double.MAX_VALUE;

            System.out.println("Problem = [" + problemName + "] Run = [" + runNumber.incrementAndGet() + "]");

            AbstractCandidateFactory<List<Integer>> factory = new JobShopFactory(jobs, times[0].length);
            FitnessEvaluator<List<Integer>> targetFitness = new FlowTimeFitness(max, times, machines);
            EvolutionaryOperator<List<Integer>> mutation = new PositionBasedMutation();
            NSGA2MulticriteriaSlow<List<Integer>> algo = new NSGA2MulticriteriaSlow<>(targetFitness, evaluators,
                    factory, mutation, new GeneralizedOrderCrossover(new Probability(CROSSOVER_PROBABILITY), jobs), CROSSOVER_PROBABILITY, POPULATION_SIZE, FastRandom.threadLocal());

            int iterPerHelper = (totalGenerations + evaluators.size() - 1) / evaluators.size();
            for (int currentGeneration = 0; currentGeneration < totalGenerations; ++currentGeneration) {
                if (currentGeneration % iterPerHelper == 0) {

                    double[] allFeatures = JobshopFeaturesUtility.computeAllFeatures(-1, metaFeatures, algo.getCurrentGeneration(), currentGeneration, problem, bestKnown);
                    Instances instances = FeatureUtility.getEmptyTrainInstances(helpersQuant);
                    instances.add(new DenseInstance(1, allFeatures));
                    double predictedHelperIndex = 0;
                    try {
                        predictedHelperIndex = classifier.classifyInstance(instances.firstInstance());
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new RuntimeException("OOOPs");
                    }


                    int predictedHelperIdx = (int) Math.round(predictedHelperIndex) - 1;
                    predictedHelperIdx = predictedHelperIdx < 0 ? 0 : predictedHelperIdx;
                    algo.changeCriterion(predictedHelperIdx);
                }
                algo.computeValues();
                double currBest = max - algo.getBestTargetValue();


                if (currBest < bestOnRun) {
                    bestFoundAt.set(i, algo.getIterationsNumber());
                    bestOnRun = currBest;
                }

                if (!foundOptimal && (Math.round(bestKnown) >= Math.floor(currBest))) {
                    optimalFoundTimes.incrementAndGet();
                    foundOptimal = true;
                    System.out.println("Problem = [" + problemName + "] Found optimal answer = " + currBest);
                    System.out.println("Problem = [" + problemName + "] Step number = " + algo.getIterationsNumber());

                }
            }
            double result = max - algo.getBestTargetValue();
            synchronized (sync) {
                dataSet.addValue(result);
                runResults.add(result);
            }
            System.out.println("Problem = [" + problemName + "]  Best = " + result);
        });
        double mean = dataSet.getArithmeticMean();
        double median = dataSet.getMedian();
        double percent = (mean - bestKnown) / bestKnown * 100;

        System.out.println(String.format("Problem = [" + problemName + "] Optimal answer =" + bestKnown));
        String s = "found optimal times = " + optimalFoundTimes.intValue();
        System.out.println("Problem = [" + problemName + "] " + s);
        jsonObject.add("found_optimal_times", new JsonPrimitive(optimalFoundTimes.intValue()));


        System.out.println("Problem = [" + problemName + "] " + s);
        double bestFoundAtAvg = Math.round(bestFoundAt.stream().mapToInt(a -> a).average().orElse(-1));
        s = "best found avg step = " + bestFoundAtAvg;
        System.out.println("Problem = [" + problemName + "] " + s);
        jsonObject.add("best_found_avg_step", new JsonPrimitive(bestFoundAtAvg));

        double minimum = dataSet.getMinimum();
        double maximum = dataSet.getMaximum();
        double standardDeviation = dataSet.getStandardDeviation();
        s = String.format("percent = %f\naverage = %f\nmedian = %f\nmin = %f\nmax = %f\ndev = %f",
                percent, mean, median, minimum, maximum, standardDeviation);
        System.out.println("Problem = [" + problemName + "] " + s);
        jsonObject.add("percent", new JsonPrimitive(percent));
        jsonObject.add("dev", new JsonPrimitive(standardDeviation));
        jsonObject.add("average", new JsonPrimitive(mean));
        jsonObject.add("median", new JsonPrimitive(median));
        jsonObject.add("max", new JsonPrimitive(maximum));
        jsonObject.add("min", new JsonPrimitive(minimum));

        JsonArray jsonElements = new JsonArray();
        for (double result : runResults) {
            jsonElements.add(new JsonPrimitive(result));
        }

        jsonObject.add("runResults", jsonElements);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        pw.println(gson.toJson(jsonObject));
        pw.flush();
    }

}
