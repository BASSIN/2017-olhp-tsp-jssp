/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.jssp;

import org.uncommons.maths.statistics.DataSet;
import ru.ifmo.ctd.olhp.core.EvaluatedIndividual;
import ru.ifmo.ctd.olhp.core.NSGA2MulticriteriaSlow;
import ru.ifmo.ctd.olhp.core.jssp.DataFileReader;
import ru.ifmo.ctd.olhp.core.jssp.JobShopUtils;
import weka.core.Attribute;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class JobshopFeaturesUtility {

    public static double[] computeMetaFeatures(DataFileReader.InputDataSet problem) {

        final int[][] machines = problem.getMachines();
        final int[][] times = problem.getTimes();

        int machinesCount = machines[0].length;
        int jobsCount = machines.length;

        double jobsMachinesRatio = (double) machinesCount / jobsCount;

        int minTime = times[0][0];
        int maxTime = times[0][0];
        double totalSum = 0;
        int totalQuant = 0;
        for (int[] t : times) {
            for (int v : t) {
                minTime = Math.min(minTime, v);
                maxTime = Math.max(maxTime, v);
                totalSum += v;
                ++totalQuant;
            }
        }

        double meanTime = totalSum / totalQuant;

        double squaredDiffs = 0;
        for (int[] t : times) {
            for (int v : t) {
                double diff = meanTime - v;
                squaredDiffs += (diff * diff);
            }
        }

        double standardDeviationTimeCost = Math.sqrt(squaredDiffs / totalQuant);


        double[] machineTimesAvg = new double[machinesCount];

        for (int i = 0; i < times.length; ++i) {
            for (int j = 0; j < times[i].length; ++j) {
                int machineIdx = machines[i][j];
                machineTimesAvg[machineIdx] += times[i][j];
            }
        }

        for (int i = 0; i < machineTimesAvg.length; i++) {
            machineTimesAvg[i] /= jobsCount;
        }

        double avgTimePerMachine = Arrays.stream(machineTimesAvg).average().getAsDouble();

        return new double[]{machinesCount, jobsCount, minTime, maxTime, jobsMachinesRatio, meanTime, standardDeviationTimeCost, avgTimePerMachine};
    }


    public static double[] computeAllFeatures(int currCriteria, double[] metaFeatures, List<NSGA2MulticriteriaSlow<List<Integer>>.Individual<List<Integer>>> currentGeneration, int generationsCount, DataFileReader.InputDataSet inputData, double bestKnown) {
        double[] statisticalFeatures = JobshopFeaturesUtility.computeStatisticalFeatures(currentGeneration, generationsCount, inputData, bestKnown);
        double[] allFeatures = new double[metaFeatures.length + statisticalFeatures.length + 1];

        allFeatures[0] = currCriteria;
        int i = 1;
        for (double metaFeature : metaFeatures) {
            allFeatures[i] = metaFeature;
            ++i;
        }
        for (double statisticalFeature : statisticalFeatures) {
            allFeatures[i] = statisticalFeature;
            ++i;
        }
        return allFeatures;
    }


    public static double[] computeStatisticalFeatures(List<NSGA2MulticriteriaSlow<List<Integer>>.Individual<List<Integer>>> currentGeneration, int generationsCount, DataFileReader.InputDataSet inputData, double bestKnown) {

        DataSet dataSet = new DataSet();

        final int[][] times = inputData.getTimes();
        final int jobs = times.length;
        final int problemMax = jobs * JobShopUtils.sumTimes(times);


        for (EvaluatedIndividual ind : currentGeneration) {
            double fitnessValue = problemMax - ind.par().getCriteria()[0];
            double concernValue = (fitnessValue / bestKnown);
            dataSet.addValue(concernValue);
        }
        double median = dataSet.getMedian();

        double arithmeticMean = dataSet.getArithmeticMean();
        double harmonicMean = dataSet.getHarmonicMean();

        double meanDeviation = dataSet.getMeanDeviation();
        double standardDeviation = dataSet.getStandardDeviation();
        double sampleStandardDeviation = dataSet.getSampleStandardDeviation();

        return new double[]{median, arithmeticMean, harmonicMean, meanDeviation, standardDeviation, sampleStandardDeviation};
    }

    public static Instances getEmptyTrainInstances(int helpersCount) {
        ArrayList<Attribute> attributes = new ArrayList<>();

        attributes.add(new Attribute("Criteria index", IntStream.range(1, helpersCount + 1).boxed().map(s -> "" + s).collect(Collectors.toList()), 0));

        //meta
        attributes.add(new Attribute("Machines Count"));
        attributes.add(new Attribute("Jobs Count"));
        attributes.add(new Attribute("Min Time"));
        attributes.add(new Attribute("Max Time"));
        attributes.add(new Attribute("Machines jobs Ration"));
        attributes.add(new Attribute("Mean Time"));
        attributes.add(new Attribute("Standard Deviation Time Cost"));
        attributes.add(new Attribute("Avg Time per Machine"));


        //local
        attributes.add(new Attribute("Median"));
        attributes.add(new Attribute("Arithmetic Mean"));
        attributes.add(new Attribute("Harmonic Mean"));
        attributes.add(new Attribute("Mean Deviation"));
        attributes.add(new Attribute("Standard Deviation"));
        attributes.add(new Attribute("Sample Standard Deviation"));
        //attributes.add(new Attribute("Number of generations"));

        Instances emptySet = new Instances("tsp_train_data", attributes, 0);
        emptySet.setClassIndex(0);
        return emptySet;
    }
}
