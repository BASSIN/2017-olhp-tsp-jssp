/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.jssp;

import org.uncommons.watchmaker.framework.FitnessEvaluator;
import ru.ifmo.ctd.olhp.core.jssp.DataFileReader;
import ru.ifmo.ctd.olhp.core.jssp.MultiFlowTimeFitness;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class UtilJobShop {

    public static int calculateEvals(int populationSize) {
        return populationSize * 200;
    }

    public static int calculateTotalIterations(DataFileReader.InputDataSet problem) {
        final int[][] machines = problem.getMachines();
        int machinesCount = machines[0].length;
        int jobsCount = machines.length;
        return machinesCount * jobsCount * 2;
    }

    public static String formatTime(long time) {
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(time),
                TimeUnit.MILLISECONDS.toMinutes(time) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)), // The change is in this line
                TimeUnit.MILLISECONDS.toSeconds(time) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
    }

    public static List<FitnessEvaluator<List<Integer>>> getEvaluators(int helpersCount, int[] helpers,
                                                                      int max, int[][] times, int[][] machines) {
        List<FitnessEvaluator<List<Integer>>> evaluators = new ArrayList<>();
        int jobs = times.length;
        int jobPerHelper = jobs / helpersCount;
        for (int j = 0; j < helpersCount; ++j) {
            evaluators.add(new MultiFlowTimeFitness(
                    Arrays.copyOfRange(helpers, j * jobPerHelper,
                            (j + 1) * jobPerHelper + (j == helpersCount - 1 ? jobs - jobPerHelper * helpersCount : 0)),
                    max, times, machines));
        }
        return evaluators;
    }

    public static double[] getMinFlowtime(int[][] times) {
        int jobs = times.length;
        double[] flowtime = new double[jobs + 1];
        int sum = 0;
        for (int j = 0; j < jobs; j++) {
            for (int k = 0; k < times[j].length; k++) {
                flowtime[j + 1] += times[j][k];
                sum += times[j][k];
            }
        }
        flowtime[0] = sum;
        return flowtime;
    }

    public static int[] sortedByFlowtime(double[] flowtime) {
        int[] sortedHelpers = new int[flowtime.length - 1];
        boolean[] usedHelper = new boolean[sortedHelpers.length];
        for (int i = 0; i < sortedHelpers.length; ++i) {
            double min = Double.MAX_VALUE;
            int pos = 0;
            for (int j = 1; j < flowtime.length; ++j) {
                if (flowtime[j] < min && !usedHelper[j - 1]) {
                    min = flowtime[j];
                    pos = j - 1;
                }
            }
            sortedHelpers[i] = pos;
            usedHelper[pos] = true;
        }
        return sortedHelpers;
    }

}
