/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.tsp;

import org.uncommons.maths.statistics.DataSet;
import ru.ifmo.ctd.olhp.core.EvaluatedIndividual;
import ru.ifmo.ctd.olhp.core.NSGA2Multicriteria;
import ru.ifmo.ctd.olhp.core.tsp.TSPProblem;
import weka.core.Attribute;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FeatureUtility {

    public static double[] computeMetaFeatures(TSPProblem problem) {

        int verticesCount = problem.getSize();

        double[][] tsp = problem.tsp;

        double minEdge = Double.MAX_VALUE;
        double maxEdge = Double.MIN_VALUE;
        double total = 0;
        double product = 1;
        double reciprocalSum = 0;
        double edgesCount = 0;
        List<Double> edgesList = new ArrayList<>();
        for (int j = 0; j < tsp.length; ++j) {
            double[] tspRow = tsp[j];
            for (int i = 0; i < tspRow.length; i++) {
                if (i == j) {
                    continue;
                }
                double edge = tspRow[i];
                edgesCount++;
                minEdge = Math.min(minEdge, edge);
                maxEdge = Math.max(maxEdge, edge);
                total += edge;
                product *= edge;
                reciprocalSum += 1 / edge;
                edgesList.add(edge);
            }
        }


        double meanEdge = total / edgesCount;

        double squaredDiffs = 0;
        for (double edge : edgesList) {
            double diff = meanEdge - edge;
            squaredDiffs += (diff * diff);
        }
        double standardDeviationEdgeCost = Math.sqrt(squaredDiffs / edgesCount);

        Collections.sort(edgesList);
        int midPoint = edgesList.size() / 2;
        double medianEdgeCost;
        if (edgesList.size() % 2 != 0) {
            medianEdgeCost = edgesList.get(midPoint);
        } else {
            medianEdgeCost = edgesList.get(midPoint - 1) + (edgesList.get(midPoint) - edgesList.get(midPoint) - 1) / 2;
        }

        double sumVedges = 0;
        for (int i = 0; i < verticesCount; i++) {
            sumVedges += edgesList.get(i);
        }
        int edgesBelowAVG = 0;
        for (double edge : edgesList) {
            if (edge < meanEdge) {
                edgesBelowAVG++;
            } else {
                break;
            }
        }

        return new double[]{verticesCount, minEdge, maxEdge, meanEdge, standardDeviationEdgeCost, medianEdgeCost, edgesBelowAVG, sumVedges};
    }


    public static double[] computeAllFeatures(int currCriteria, double[] metaFeatures, List<NSGA2Multicriteria<List<Integer>>.Individual<List<Integer>>> currentGeneration, int generationsCount, TSPProblem problem) {
        double[] statisticalFeatures = FeatureUtility.computeStatisticalFeatures(currentGeneration, generationsCount, problem);
        double[] allFeatures = new double[metaFeatures.length + statisticalFeatures.length + 1];

        allFeatures[0] = currCriteria;
        int i = 1;
        for (double metaFeature : metaFeatures) {
            allFeatures[i] = metaFeature;
            ++i;
        }
        for (double statisticalFeature : statisticalFeatures) {
            allFeatures[i] = statisticalFeature;
            ++i;
        }
        return allFeatures;
    }


    public static double[] computeStatisticalFeatures(List<NSGA2Multicriteria<List<Integer>>.Individual<List<Integer>>> currentGeneration, int generationsCount, TSPProblem problem) {

        double bestKnown = problem.solution;

        DataSet dataSet = new DataSet();

        double problemMax = problem.getMax();
        for (EvaluatedIndividual ind : currentGeneration) {
            double fitnessValue = problemMax - ind.par().getCriteria()[0];
            double concernValue = (fitnessValue / bestKnown);
            dataSet.addValue(concernValue);
        }
        double median = dataSet.getMedian();

        double arithmeticMean = dataSet.getArithmeticMean();
        double harmonicMean = dataSet.getHarmonicMean();

        double meanDeviation = dataSet.getMeanDeviation();
        double standardDeviation = dataSet.getStandardDeviation();
        double sampleStandardDeviation = dataSet.getSampleStandardDeviation();

        return new double[]{median, arithmeticMean, harmonicMean, meanDeviation, standardDeviation, sampleStandardDeviation};

    }

    public static Instances getEmptyTrainInstances(int helpersCount) {
        ArrayList<Attribute> attributes = new ArrayList<>();

        attributes.add(new Attribute("Criteria index", IntStream.range(1, helpersCount + 1).boxed().map(s -> "" + s).collect(Collectors.toList()), 0));

        //meta
        attributes.add(new Attribute("Vertices Count"));
        attributes.add(new Attribute("Min Edge"));
        attributes.add(new Attribute("Max Edge"));
        attributes.add(new Attribute("Mean Edge"));
        attributes.add(new Attribute("Standard Deviation Edge Cost"));
        attributes.add(new Attribute("Median Edge Cost"));
        attributes.add(new Attribute("Edges Below AVG"));
        attributes.add(new Attribute("Sum V Edges"));

        //local
        attributes.add(new Attribute("Median"));
        attributes.add(new Attribute("Arithmetic Mean"));
        attributes.add(new Attribute("Harmonic Mean"));
        attributes.add(new Attribute("Mean Deviation"));
        attributes.add(new Attribute("Standard Deviation"));
        attributes.add(new Attribute("Sample Standard Deviation"));
        //attributes.add(new Attribute("Number of generations"));

        Instances emptySet = new Instances("tsp_train_data", attributes, 0);
        emptySet.setClassIndex(0);
        return emptySet;
    }
}
