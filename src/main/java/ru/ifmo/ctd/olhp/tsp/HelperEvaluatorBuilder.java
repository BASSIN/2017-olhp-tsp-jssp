/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.tsp;

import org.apache.commons.math3.util.Pair;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import ru.ifmo.ctd.olhp.core.tsp.TSPProblem;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class HelperEvaluatorBuilder {

    private final int K;
    private final TSPProblem PROBLEM;
    private List<Pair<Integer, Double>> pointsDistance = new ArrayList<>();

    public HelperEvaluatorBuilder(TSPProblem problem, int k) {
        this.K = k;
        this.PROBLEM = problem;
        clusterize();
    }

    private void clusterize() {
        for (int i = 0; i < PROBLEM.getSize(); i++) {
            double distanceNearest = 0;
            for (int j = 0; j < K; j++) {
                final int neighbour = PROBLEM.getTSPNeighbour(i, j);
                distanceNearest += PROBLEM.getTSPValue(i, neighbour);
            }
            pointsDistance.add(new Pair<>(i, distanceNearest));
        }
        pointsDistance.sort(Comparator.comparingDouble(Pair::getSecond));
    }

    public List<FitnessEvaluator<List<Integer>>> getEvaluators(int helpersCount) {
        List<FitnessEvaluator<List<Integer>>> evaluators = new ArrayList<>();
        int partitionSize = pointsDistance.size() / helpersCount;
        for (int i = 0, j = 0; i < pointsDistance.size() && j < helpersCount; i += partitionSize, ++j) {
            int end = j == helpersCount - 1 ? pointsDistance.size() : i + partitionSize;
            final List<Integer> pointsList = pointsDistance.subList(i, end).stream().map(Pair::getFirst).collect(Collectors.toList());
            evaluators.add(new TSPJahneFitness(PROBLEM, pointsList));
        }
        return evaluators;
    }
}
