/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.tsp;

import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.xml.sax.SAXException;
import ru.ifmo.ctd.olhp.UtilsOLHP;
import ru.ifmo.ctd.olhp.core.FastRandom;
import ru.ifmo.ctd.olhp.core.NSGA2Multicriteria;
import ru.ifmo.ctd.olhp.core.tsp.TSPCrossover;
import ru.ifmo.ctd.olhp.core.tsp.TSPFactory;
import ru.ifmo.ctd.olhp.core.tsp.TSPFitness;
import ru.ifmo.ctd.olhp.core.tsp.TSPMutation;
import ru.ifmo.ctd.olhp.core.tsp.TSPNSGA2Multicriteria;
import ru.ifmo.ctd.olhp.core.tsp.TSPProblem;
import ru.ifmo.ctd.olhp.core.tsp.TSPUtils;
import weka.classifiers.AbstractClassifier;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class TSPClassifierBuilderOLHP {

    private static final Random RANDOM = FastRandom.threadLocal();
    public static final int M = 10;
    private static final int HELPERS_COUNT = 5;
    private static final int NUMBER_OF_RUNS = 4;
    public static final double CROSSOVER_PROBABILITY = 0.4;
    public static final int K = 5;

    private static Instances trainData;

    private static AbstractClassifier classifier;


    public static int populationSize = 100;

    public static void runLearning(String dataFileDirectory, String trainInstancesFilePath) throws Exception {
        Map<String, File> filesMap = UtilsOLHP.getFilesMapFromDir(dataFileDirectory);
        if (filesMap == null || filesMap.isEmpty()) {
            throw new RuntimeException("No train files!!!");
        }

        List<String> trainProblems = UtilsOLHP.readProblemList(trainInstancesFilePath);

        classifier = UtilsOLHP.getClassifier();
        trainData = FeatureUtility.getEmptyTrainInstances(HELPERS_COUNT);

        long startTime = System.currentTimeMillis();
        final int totalFiles = trainProblems.size();

        AtomicInteger filesLeft = new AtomicInteger(totalFiles);
        IntStream.range(0, totalFiles).parallel().forEach(i ->
        {
            try {
                File trainFile = filesMap.get(trainProblems.get(i));
                System.out.println("Processing " + trainFile.getName() + ". Files left = " + filesLeft.decrementAndGet());
                System.out.println("Total runs quantity =  " + NUMBER_OF_RUNS);
                runTraining(trainFile, populationSize, NUMBER_OF_RUNS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        long endTime = System.currentTimeMillis();
        System.out.println("TOTAL TIME = [" + formatTime(endTime - startTime) + "]");

        System.out.println("Randomizing dataset");
        trainData.randomize(RANDOM);

        System.out.println("Number of dataset vectors = [" + trainData.numInstances() + "]");

        System.out.println("Building classifier");
        classifier.buildClassifier(trainData);

        Date date = new Date();
        String directoryPath = "./train_output/TSP/" + new SimpleDateFormat("yy-MM-dd-HH-mm-ss").format(date);
        new File(directoryPath).mkdirs();

        UtilsOLHP.storeClassifier(classifier, directoryPath);
        UtilsOLHP.storeEvaluatorsNumber(HELPERS_COUNT, directoryPath);
        UtilsOLHP.storeData(trainData, directoryPath);


        UtilsOLHP.crossValidate(classifier, trainData, directoryPath);

    }

    private static void runTraining(File trainFile, int populationSize, int runs) throws IOException, ParserConfigurationException, SAXException {
        long startTime = System.currentTimeMillis();

        TSPProblem tspProblem = TSPUtils.readXMLInstance(trainFile.getCanonicalPath());
        final double[] metaFeatures = FeatureUtility.computeMetaFeatures(tspProblem);
        final int evals = (int) Math.sqrt(Math.pow(tspProblem.getSize(), 3)) * M;
        final int totalGenerations = evals / populationSize;
        System.out.println(String.format("Optimal answer is %.3f", tspProblem.solution));


        AtomicInteger runNumber = new AtomicInteger(0);
        IntStream.range(0, runs).parallel().forEach(i -> {

            TSPProblem problem = new TSPProblem(tspProblem.tsp, tspProblem.solution);
            final HelperEvaluatorBuilder helperEvaluatorBuilder = new HelperEvaluatorBuilder(problem, K);
            List<FitnessEvaluator<List<Integer>>> evaluators = helperEvaluatorBuilder.getEvaluators(HELPERS_COUNT);

            FitnessEvaluator<List<Integer>> targetFitness = new TSPFitness(problem);
            TSPMutation mutation = new TSPMutation();
            TSPCrossover crossover = new TSPCrossover(CROSSOVER_PROBABILITY);

            final String logPrefix = "Problem [" + trainFile.getPath() + "] Run [" + runNumber.incrementAndGet() + "] ";
            System.out.println(logPrefix + "Current time: " + new Date().toString());
            long runStartTime = System.currentTimeMillis();

            TSPFactory factory = new TSPFactory(problem);
            TSPNSGA2Multicriteria algo =
                    new TSPNSGA2Multicriteria(targetFitness, evaluators, factory, mutation, crossover, CROSSOVER_PROBABILITY, populationSize, FastRandom.threadLocal(), problem);

            int iterPerHelper = (totalGenerations + evaluators.size() - 1) / evaluators.size();

            int currGenerationsCount = 0;
            while (currGenerationsCount < totalGenerations) {

                List<NSGA2Multicriteria<List<Integer>>.Individual<List<Integer>>> oldGeneration = algo.getCurrentGeneration();
                int oldIterations = algo.getIterationsNumber();

                double bestFitness = Double.NEGATIVE_INFINITY;
                List<NSGA2Multicriteria<List<Integer>>.Individual<List<Integer>>> bestGeneration = null;
                int bestIterations = -1;
                int bestCriteria = -1;

                for (int k = 0; k < evaluators.size(); k++) {
                    algo.changeCriterion(k);
                    for (int j = 0; j < iterPerHelper; ++j) {
                        algo.computeValues();
                    }

                    double bestTargetValue = algo.getCurrentBest().get(0);

                    if (bestTargetValue > bestFitness) {
                        bestCriteria = k;
                        bestIterations = algo.getIterationsNumber();
                        bestGeneration = algo.getCurrentGeneration();
                        bestFitness = bestTargetValue;
                    }
                    if (k < evaluators.size() - 1) {
                        algo.restoreToState(oldGeneration, oldIterations);
                    }
                }

                addFeaturesVector(bestCriteria, metaFeatures, oldGeneration, currGenerationsCount, problem);


                currGenerationsCount += iterPerHelper;
                algo.restoreToState(bestGeneration, bestIterations);
            }
            long endTime = System.currentTimeMillis();
            long totalTime = endTime - runStartTime;
            String format = formatTime(totalTime);
            System.out.println(logPrefix + " time_taken = [" + format + "]");
        });

        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        String format = formatTime(totalTime);
        System.out.println("trainFile = [" + trainFile.getName() + "], time_taken_total = [" + format + "]");
    }


    private static String formatTime(long time) {
        return String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(time),
                TimeUnit.MILLISECONDS.toMinutes(time) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)), // The change is in this line
                TimeUnit.MILLISECONDS.toSeconds(time) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
    }

    private static synchronized void addFeaturesVector(int currCriteria, double[] metaFeatures, List<NSGA2Multicriteria<List<Integer>>.Individual<List<Integer>>> currentGeneration, int generationsCount, TSPProblem problem) {
        double[] allFeatures = FeatureUtility.computeAllFeatures(currCriteria, metaFeatures, currentGeneration, generationsCount, problem);
        Instance instance = new DenseInstance(1, allFeatures);
        trainData.add(instance);
    }
}
