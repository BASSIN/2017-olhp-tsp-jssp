/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.tsp;

import org.uncommons.watchmaker.framework.FitnessEvaluator;
import ru.ifmo.ctd.olhp.core.tsp.TSPProblem;

import java.io.Serializable;
import java.util.List;

public class TSPJahneFitness implements FitnessEvaluator<List<Integer>>, Serializable {
    private final double max;
    private final TSPProblem problem;
    private final List<Integer> points;

    public TSPJahneFitness(TSPProblem problem, List<Integer> points) {
        this.points = points;
        this.max = problem.getMax();
        this.problem = problem;
    }

    @Override
    public double getFitness(List<Integer> individual, List<? extends List<Integer>> population) {
        double flowTimes = 0;
        for (int p : points) {
            int pointPos = 0;
            for (int i = 0; i < individual.size(); ++i) {
                if (p == individual.get(i)) {
                    pointPos = i;
                    break;
                }
            }
            int cPrevPos = pointPos == 0 ? individual.size() - 1 : pointPos - 1;
            int cNextPos = pointPos == individual.size() - 1 ? 0 : pointPos + 1;
            int cPrev = individual.get(cPrevPos);
            int cNext = individual.get(cNextPos);
            flowTimes += problem.tsp[cPrev][p] + problem.tsp[p][cNext];
        }

        if (flowTimes == Double.POSITIVE_INFINITY) {
            flowTimes = 0;
        }
        return max - flowTimes;
    }

    @Override
    public boolean isNatural() {
        return true;
    }

}

