/*
 * (C) Copyright 2017 Anton Bassin and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *     Anton Bassin
 *     Arina Buzdalova
 *     Maxim Buzdalov
 *     Irene Petrova
 */

package ru.ifmo.ctd.olhp.tsp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.uncommons.maths.statistics.DataSet;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import ru.ifmo.ctd.olhp.UtilsOLHP;
import ru.ifmo.ctd.olhp.core.FastRandom;
import ru.ifmo.ctd.olhp.core.tsp.TSPCrossover;
import ru.ifmo.ctd.olhp.core.tsp.TSPFactory;
import ru.ifmo.ctd.olhp.core.tsp.TSPFitness;
import ru.ifmo.ctd.olhp.core.tsp.TSPMutation;
import ru.ifmo.ctd.olhp.core.tsp.TSPNSGA2Multicriteria;
import ru.ifmo.ctd.olhp.core.tsp.TSPProblem;
import ru.ifmo.ctd.olhp.core.tsp.TSPUtils;
import weka.classifiers.AbstractClassifier;
import weka.core.DenseInstance;
import weka.core.Instances;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class TSPRunnerOLHP {

    private static final int M = 10;
    private static final double CROSSOVER_PROBABILITY = 0.4;

    public static final int POPULATION_SIZE = 100;
    public static final int RUNS = 40;


    public static void run(String dataFileDir, String testInstancesFilePath, String trainDataPath) throws IOException, ClassNotFoundException, URISyntaxException {

        int helpersQuant = UtilsOLHP.readHelpersQuant(trainDataPath);
        AbstractClassifier classifier = UtilsOLHP.readClassifier(trainDataPath);

        Map<String, File> filesMap = UtilsOLHP.getFilesMapFromDir(dataFileDir);
        if (filesMap == null || filesMap.isEmpty()) {
            throw new RuntimeException("No test files!!!");
        }

        List<String> testProblems = UtilsOLHP.readProblemList(testInstancesFilePath);

        String outPutDirectoryPath = "./test_results/TSP/" + new SimpleDateFormat("yy-MM-dd-HH-mm-ss").format(new Date());
        new File(outPutDirectoryPath).mkdirs();

        final int totalFiles = testProblems.size();
        AtomicInteger filesLeft = new AtomicInteger(totalFiles);
        IntStream.range(0, totalFiles).forEach(i ->
        {
            try {
                File testFile = filesMap.get(testProblems.get(i));
                System.out.println("Processing " + testFile.getName() + ". Files left = " + (filesLeft.decrementAndGet()));
                String problemName = testFile.getName().split("\\.")[0];
                TSPProblem problem = TSPUtils.readXMLInstance(testFile.getCanonicalPath());

                final HelperEvaluatorBuilder helperEvaluatorBuilder = new HelperEvaluatorBuilder(problem, helpersQuant);
                List<FitnessEvaluator<List<Integer>>> evaluators = helperEvaluatorBuilder.getEvaluators(helpersQuant);
                runTSPWithOLHP(problem, problemName, helpersQuant, evaluators, classifier, outPutDirectoryPath, POPULATION_SIZE, RUNS);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private static void runTSPWithOLHP(TSPProblem problem, String problemName, final int helpersQuant, List<FitnessEvaluator<List<Integer>>> evaluators, AbstractClassifier classifier, String outPutDirectoryPath, int populationSize, int runs) throws Exception {
        PrintWriter pw = new PrintWriter(outPutDirectoryPath + "/" + problemName + "-" + new SimpleDateFormat("yy-MM-dd-HH-mm-ss").format(new Date()) + "-OLHP.json");
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("problem_name", problemName);
        jsonObject.addProperty("method", "OLHP");
        final int evals = (int) Math.sqrt(Math.pow(problem.getSize(), 3)) * M;
        final int totalGenerations = evals / populationSize;

        double[] metaFeatures = FeatureUtility.computeMetaFeatures(problem);
        System.out.println(String.format("Problem = [" + problemName + "] Optimal answer = %.3f", problem.solution));

        jsonObject.add("optimal_answer", new JsonPrimitive(problem.solution));
        DataSet dataSet = new DataSet();
        List<Integer> optimalFoundSteps = new ArrayList<>();
        List<Integer> bestFoundAt = new CopyOnWriteArrayList<>(Collections.nCopies(runs, -1));

        List<Double> runResults = new CopyOnWriteArrayList<>();
        AtomicInteger runNumber = new AtomicInteger(0);
        Object sync = new Object();
        IntStream.range(0, runs).parallel().forEach(i -> {
            boolean foundOptimal = false;
            double bestOnRun = Double.MAX_VALUE;

            System.out.println("Problem = [" + problemName + "] Run = [" + runNumber.incrementAndGet() + "]");
            FitnessEvaluator<List<Integer>> targetFitness = new TSPFitness(problem);
            TSPFactory factory = new TSPFactory(problem);
            TSPMutation mutation = new TSPMutation();
            TSPCrossover crossover = new TSPCrossover(CROSSOVER_PROBABILITY);
            TSPNSGA2Multicriteria algo =
                    new TSPNSGA2Multicriteria(targetFitness, evaluators, factory, mutation, crossover, CROSSOVER_PROBABILITY, populationSize, FastRandom.threadLocal(), problem);

            int iterPerHelper = (totalGenerations + evaluators.size() - 1) / evaluators.size();
            for (int currentGeneration = 0; currentGeneration < totalGenerations; ++currentGeneration) {
                if (currentGeneration % iterPerHelper == 0) {

                    double[] allFeatures = FeatureUtility.computeAllFeatures(-1, metaFeatures, algo.getCurrentGeneration(), currentGeneration, problem);
                    Instances instances = FeatureUtility.getEmptyTrainInstances(helpersQuant);
                    instances.add(new DenseInstance(1, allFeatures));
                    double predictedHelperIndex = 0;
                    try {
                        predictedHelperIndex = classifier.classifyInstance(instances.firstInstance());
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new RuntimeException("OOOPs");
                    }


                    int predictedHelperIdx = (int) Math.round(predictedHelperIndex) - 1;
                    predictedHelperIdx = predictedHelperIdx < 0 ? 0 : predictedHelperIdx;
                    algo.changeCriterion(predictedHelperIdx);
                }
                algo.computeValues();
                double currBest = problem.getMax() - algo.getBestTargetValue();


                if (currBest < bestOnRun) {
                    bestFoundAt.set(i, algo.getIterationsNumber());
                    bestOnRun = currBest;
                }

                if (!foundOptimal && (Math.round(problem.solution) >= Math.floor(currBest))) {
                    optimalFoundSteps.add(algo.getIterationsNumber());
                    foundOptimal = true;
                    System.out.println("Problem = [" + problemName + "] Found optimal answer = " + currBest);
                    System.out.println("Problem = [" + problemName + "] Step number = " + algo.getIterationsNumber());

                }
            }
            double result = problem.getMax() - algo.getBestTargetValue();
            synchronized (sync) {
                dataSet.addValue(result);
                runResults.add(result);
            }
            System.out.println("Problem = [" + problemName + "]  Best = " + result);
        });
        double mean = dataSet.getArithmeticMean();
        double median = dataSet.getMedian();
        double percent = (mean - problem.solution) / problem.solution * 100;

        System.out.println(String.format("Problem = [" + problemName + "] Optimal answer = %.3f", problem.solution));
        String s = "found optimal times = " + optimalFoundSteps.size();
        System.out.println("Problem = [" + problemName + "] " + s);
        jsonObject.add("found_optimal_times", new JsonPrimitive(optimalFoundSteps.size()));

        double found_optimal_times = Math.round(optimalFoundSteps.stream().mapToInt(a -> a).average().orElse(-1));
        s = "found optimal avg step = " + found_optimal_times;
        System.out.println("Problem = [" + problemName + "] " + s);
        jsonObject.add("found_optimal_avg_step", new JsonPrimitive(found_optimal_times));

        double bestFoundAtAvg = Math.round(bestFoundAt.stream().mapToInt(a -> a).average().orElse(-1));
        s = "best found avg step = " + bestFoundAtAvg;
        System.out.println("Problem = [" + problemName + "] " + s);
        jsonObject.add("best_found_avg_step", new JsonPrimitive(bestFoundAtAvg));

        double minimum = dataSet.getMinimum();
        double maximum = dataSet.getMaximum();
        double standardDeviation = dataSet.getStandardDeviation();
        s = String.format("percent = %f\naverage = %f\nmedian = %f\nmin = %f\nmax = %f\ndev = %f",
                percent, mean, median, minimum, maximum, standardDeviation);
        System.out.println("Problem = [" + problemName + "] " + s);

        jsonObject.add("percent", new JsonPrimitive(percent));
        jsonObject.add("dev", new JsonPrimitive(standardDeviation));
        jsonObject.add("average", new JsonPrimitive(mean));
        jsonObject.add("median", new JsonPrimitive(median));
        jsonObject.add("max", new JsonPrimitive(maximum));
        jsonObject.add("min", new JsonPrimitive(minimum));

        JsonArray jsonElements = new JsonArray();
        for (double result : runResults) {
            jsonElements.add(new JsonPrimitive(result));
        }

        jsonObject.add("runResults", jsonElements);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        pw.println(gson.toJson(jsonObject));
        pw.flush();
    }
}
